---
Title: "SysAdminDay 2020"
Date: 2020-07-15T20:34:04+02:00
Draft: false
Type: "post"
Summary: "SysAdminDay 2020"
Slug: "SysAdminDay2020"
Author: "Meskó Balázs"
---

Idén is részt veszünk a HWSW szervezésében megrendezett SysAdminDayen. Az esemény július 17.-én, 14:15-kor kezdődik, az óbudai [Budapest Gardenben][1]. A standunk mellett a szokásos nyereményjátékunkkal is készülünk, idén számos egyedi bögrét sorsolunk ki, illetve fődíjként egy Kindle PaperWhite is gazdára talál.

Csatlakozzatok ti is, beszélgessünk, és igyunk egy sört a rendszergazdák egészségére :-)

> „Éljetek szabadon, használjatok szabad szoftvert!”

{{< blogbutton label="Regisztráció és további részletek" href="https://rendezveny.hwsw.hu/sysadminday/6/?ref=FSF" >}}

[1]: https://www.openstreetmap.org/?mlat=47.53093&amp;mlon=19.04264#map=17/47.53093/19.04264
