---
Title: "Így készült a Rocket.Chat honosítása"
Date: 2022-12-20T10:00:00+02:00
Draft: false
Type: "post"
Summary: ""
Slug: "RocketChat-honositas"
Author: "Úr Balázs"
---

Az FSF.hu Alapítványtól a „Szabad Szoftver Pályázat 2022” keretében elnyert támogatással elkészült a Rocket.Chat honosítása. Nemcsak a hiányzó karakterláncokat kellett lefordítani, hanem a meglévőket is át kellett nézni, ugyanis a meglévő nyelvi fájl gépi fordítással készült. A fordítás az ütemtervnek megfelelően, időben elkészült.

![A Rocket.Chat honosítása és a fordítás lektorálása]({{< staticref "assets/images/blog/2022/rocket-chat-progress.png">}} "A Rocket.Chat honosítása és a fordítás lektorálása")

A fordítást [Kiss Kinga](https://kisskinga.hu/) végezte a pályázat keretében, a lektorálást pedig [Úr Balázs](https://www.urbalazs.hu/) társadalmi munkában.

Sajnos nem egyedi eset, hogy a fejlesztő megpróbál segíteni a fordítóknak a gépi fordítással, de magyar nyelvre ez még nem az igazi, és inkább árt mint használ. A fordító azt látja, hogy 90%-ban le van fordítva, ezért nem nézi át a meglévőt, csak a hiányzót pótolja. A Rocket.Chat esetén ez 78%-os készenlétet jelentett, amikor elkezdtük dolgozni rajta, de az összes fordítást át kell nézni. A tapasztalat az, hogy az egyszavas karakterláncok szinte mindig jók, a kétszavasok többé-kevésbé, a hosszabbak pedig teljesen magyartalanok.

### Hogyan készült

A Rocket.Chat JSON-fájlt használ a fordításhoz. Ez egy nagyon egyszerű, kulcs-érték párt használó formátum, például: "String_key": "Magyar fordítás".

Az általunk előnyben részesített fordítóprogram a [Lokalize](https://docs.kde.org/stable5/hu/lokalize/lokalize/index.html). A funkcióiról a magyarul is elérhető kézikönyvében található bővebb információ.

A Lokalize PO-fájlokkal dolgozik. A JSON-fájlt parancssori eszközökkel ([json2po](https://docs.translatehouse.org/projects/translate-toolkit/en/latest/commands/json2po.html)) alakítottuk át PO-fájlra, majd vissza (po2json). Ezek a parancssori eszközök jól bevált, egyszerűen használható programok.

A munkafolyamat az volt, hogy előállítottunk egy teljesen üres nyelvi fájlt, elkezdtük az elejéről fordítani úgy, hogy a Lokalize [szinkronizálási funkciójával](https://docs.kde.org/stable5/hu/lokalize/lokalize/sync.html) minden karakterlánchoz megnéztük, hogy a meglévő fordítás használható-e. Ha igen, akkor egyszerűen átmásoltuk a meglévő fordítást, ha nem, akkor bedobtuk DeepL-be, majd a kapott eredményt tettük valódi magyar mondattá. Általában csak ragozást vagy szórendet kellett javítani, sok gépeléstől kímélt meg minket. Alapvetően az a fő probléma a DeepL javaslataival, hogy az egyes karakterláncok fordítása egymás után, egymástól különállóan történt, ezért a DeepL ugyan jó fordítást ad vissza, de összességében nem lenne konzisztens a nyelvezet. Jelenleg az emberi átnézés és javítás elengedhetetlen a jó minőségű, konzisztens fordítás létrehozásához.

Próbáltunk a DeepL-nek is segíteni, és a javításokat a webes felületen végeztük el, hogy tanuljon belőle. Általában az átfogalmazni kívánt szóra kattintva az első javaslata pont az, amit inkább szerettünk volna látni.

Kontextus nélkül azokat nehéz fordítani, ahol a szó egyszerre lehet ige és főnév is. Egy „Group View Settings” lehet „Csoportnézet beállításai” és „Nézetbeállítások csoportosítása” is. Elég sok mindet fordítunk, és sokszor nincs lehetőségünk vagy időnk működés közben is kipróbálni, vagy nem találjuk meg benne a keresett karakterláncot. Emiatt van, hogy tippelni kell, ha a forráskód sem segít.

### Félrefordítások

A meglévő magyar nyelvi fájl nagyrészt gépi fordítással készült. Sajnos a Rocket.Chat fejlesztői továbbra is [gépi fordítást](https://github.com/RocketChat/Rocket.Chat/commit/96858101017a026c93a7e77dd3bd322bfbf3e715#diff-2eae51c55f80c4fe2c53e589fc57f6a43cea01513ab0b1f94d0295e6f3e8d735) használnak az új karakterláncokra. Nehezen hihető, hogy a hozzáadott új karakterláncok egyszerre az összes támogatott nyelven lefordításra kerültek, azért ennyire nem aktívak a nyelvi csapatok. Egy ember pedig nem fog ilyet fordítani:

    Mielőtt bejelentkezés fiókját manuálisan kell aktiválni a rendszergazda.

A gépi fordítás másik problémája, hogy a több jelentésű szavak a környezet ismerete nélkül elég gyakran más jelentést kapnak ahhoz képest, ami az adott környezetben elvárt. Íme néhány gyöngyszem ezek közül:

    File removed by automatic prune = A fájl eltávolítása automatikus prúzással történik
    Pruning files = Metszési fájlok
    User Left Room = A felhasználó bal oldala
    Message starring = Üzenet főszereplésével
    Prune = Aszalt szilva
    Send request on lead capture = Küldje el a kérést az ólom rögzítéskor
    Unread Count = Olvasatlan gróf

A gépi fordítás sokszor nyelvújítással is próbálkozik, és új szavakat alkot. Ez a fordítás kicsit [lórum ipsésre](http://www.lorumipse.hu/) sikerült:

    Comma-separated list of domains allowed to embed the livechat widget = Vonalkomástól elválasztott tartományok listája, amelyek lehetővé teszik az élőcsat widget beépítését

És végül vannak azok az esetek, amikor egy karakterlánc valójában több részletből áll össze. A magyar nyelvi fájlban ez a fordítás volt:

    From = Ból ből
    to = nak nek

Ebből a „From 2022-11-21 to 2022-11-22” angol mondat lett megszerkesztve, ami magyar fordítással „Ból ből 2022-11-21 nak nek 2022-11-22” lett.

A fenti hibák az átnézett és javított fordításban természetesen már nem szerepelnek. Ha valaki csemegézni szeretne a gépi fordítású szörnyűségeken, akkor a beküldött [pull request](https://github.com/RocketChat/Rocket.Chat/pull/27575) tartalmazza, hogy mi változott.

A honosítók gyakran kapnak kritikát a magyar fordítással kapcsolatban. Sokan azt vallják, hogy a szoftvereket eredeti angol nyelven kell használni, mert így az esetleges hibás fordítás nem okoz problémát. Sokszor viszont nem az angol az eredeti nyelve a szoftvernek, hanem indiai, kazak, afgán vagy ki tudja, hogy a közreműködők melyik országból származnak. A honosításellenesek figyelmébe ajánlom az alábbi, angolnak látszó mondatot:

    It True sync process will be import all LDAP users <br/> Caution! Specify search filter to not import excess users.

Az értelmetlen mondat és a hibás igealak ellenére szerintem magyarul mégiscsak érthetőbb (ha jól értelmeztük, hogy mire gondolhatott a költő):

    Ha igaz, akkor a szinkronizálási folyamat az összes LDAP-felhasználót importálni fogja<br/>Vigyázat! Adjon meg egy keresési szűrőt, hogy ne importálja a felesleges felhasználókat.

A fenti hibás angol szövegek az igazán kemény diók a gépi fordítás számára. A legtöbb értelmetlen fordítás a hibás angol szövegeknél található.

### Számok

A fordítási munkáknál sokszor jelent gondot az elvégzendő feladat költségének becslése. Általában 15 Ft/szó vagy 2,5 Ft/karakter (szóközökkel együtt) szoktunk számolni.

Nézzük akkor a számokat a Rocket.Chat fordítása esetében:

- Az angol forrásfájlban 5543 karakterlánc, 30418 szó, 191647 karakter található. Az elkészült magyar nyelvi fájlban 5543 karakterlánc, 29261 szó, 236160 karakter lett.
- Az elkészült magyar nyelvi fájlban 1263 karakterlánc, 3286 szó, 27537 karakter maradt változatlan, tehát ennyi volt a helyes fordítás.
- Ugyanez százalékban kifejezve: 22,79%-a a karakterláncoknak, 11,23%-a szavaknak, 11,66%-a a karaktereknek. Vagyis a meglévő magyar fordítás nagyjából 88,50%-a hiányzott vagy hibás volt. Itt visszautalok a bevezetőben említett 78%-ra, azaz a karakterláncok 78%-a volt géppel lefordítva, de végül csak a 11,23%-a bizonyult helyesnek.
- A fordítás 37 nap alatt készült el, átlagosan napi 2 munkaórával számolva. Ez 14,5 munkanapnak, azaz 3 heti munkának felel meg az [50/1999. (XI. 3.) EüM rendelet](https://njt.hu/jogszabaly/1999-50-20-0B) 4. §-ának maradéktalan betartásával (napi legfeljebb 6 óra képernyő előtti munkavégzés, óránként 10 perc szünettel).
- A KSH szerinti bruttó átlagkereset 504 100 Ft volt [ebben az időszakban](https://www.ksh.hu/docs/hun/xftp/gyor/ker/ker2209.html), ami 6 835 596 Ft ráfordítást jelent egy munkáltatónak. 2022-ben 254 munkanap volt, egy átlagos munkavállalónak 30 nap szabadság jár. A teljes ráfordítás és a szabadságok figyelembe vételével 457 741 Ft bérköltség jut 3 heti munkára.
- A fordítás tényleges költsége így 17,62 Ft/szó vagy 2,19 Ft/leütés.
- Egy profitorientált szervezet nyereséget is szeretne termelni, így 15% nyereséggel a 20 Ft/szó vagy 2,5 Ft/karakter reális árnak tűnik.
- Az FSF.hu Alapítványtól elnyert 350 000 Ft-os pályázati összeg a fordítás költségének 76,46%-át fedezte.
- Ennek a cikknek a megírása a benne lévő adatok előállításával együtt 4 órát vett igénybe.

### Karbantartás és visszajelzés

A Rocket.Chat fordítását Úr Balázs fogja karbantartani. A karbantartás azt jelenti, hogy új karakterláncok érkezésekor vagy a meglévő karakterláncok megváltozásakor a magyar fordítást is rendszeresen kiegészíti vagy hozzáigazítja az angol szöveghez.

Fordítási hibát találtál? Jelezd Balázsnak az ur pötty balazs giliszta fsf pötty hu e-mail-címen.

A gép (és mások) hibás fordítása ellen Balázsnak ez a munkafolyamata:

0. Összes karakterlánc átnézése, javítása, a hiányzó fordítás befejezése, az egész fordítás következetessé tétele. Ez tekinthető kész fordításnak. A Rocket.Chat fordítása jelenleg ebben az állapotban van.
1. Az utolsó, a karbantartó által átnézett nyelvi fájl használata alapként az előző mentésből.
2. Ha változott valami a forrásban, akkor az üres katalógusfájl (POT-fájl) letöltése vagy a kódtárolóban lévő magyar nyelvi fájl (PO-fájl vagy JSON-fájl) összes fordításának törlése.
3. Az új, üres nyelvi fájl szinkronizálása az 1. pontban lévő átnézett fájllal.
4. A 3. pontban előállt fájl szinkronizálása a kódtárolóban lévő magyar nyelvi fájllal (itt jönnek elő azok a fordítások, amelyeket nem a karbantartó fordított).
5. Az új fordítások átnézése és elfogadása vagy javítása.
6. A hiányzó karakterláncok fordításának befejezése.

A 0. lépést csak egyszer kell megcsinálni, az 1-6 lépést pedig mindig, ha valamilyen szöveg változott a forráskódban. Nagyjából ez lenne a feladata a fordítás karbantartójának. A [fájlok szinkronizálását](https://docs.kde.org/stable5/hu/lokalize/lokalize/sync.html), pontosabban két nyelvi fájl közötti különbséget a Lokalize-on kívül más szoftverrel nem sikerült még elvégezni, még a parancssori eszközökkel sem.