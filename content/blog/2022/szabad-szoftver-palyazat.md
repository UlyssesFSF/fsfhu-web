---
Title: "Szabad Szoftver Pályázat 2022"
Date: 2022-10-20T16:40:00+02:00
Draft: false
Type: "post"
Summary: ""
Slug: "SzabadSzoftverPalyazat2022"
Author: "Meskó Balázs"
---

Az FSF.hu Alapítvány pályázatot ír ki szabad szoftverek fejlesztése, népszerűsítése és honosítása témakörben. A pályázók köre: bármely magyarországi természetes vagy jogi személy. A pályázatra biztosított keretösszeg 4 millió forint. Egy pályázó több pályázatot is nyújthat be. Egy pályázat maximális támogatási összege 1 millió forint. A támogatott projektek várható száma: 4-12 pályázat.

Az idei évben újdonság, hogy a pályázatok kivonatát tartalmazó fedlapokat (illetve ha a beküldők hozzájárulnak, akkor a teljes pályázati anyagukat) közzé fogjuk tenni a blogunkon. Illetve a pályázat lezárultával saját összefoglalóval is készülünk az egyes pályázatok eredményéről.

A pályázatok leadási határideje november 5-e, míg a projektek elkészülésének határideje december 25-e. A részletes pályázati kiírás [innen tölthető le]({{< staticref "assets/pdf/szszp2022.pdf">}}).

Minta a pályázat fedlapjához:

* [PDF formátumban]({{< staticref "assets/pdf/szszp2022_fedlap_minta.pdf">}})
* [ODT formátumban]({{< staticref "assets/odt/szszp2022_fedlap_minta.odt">}})

