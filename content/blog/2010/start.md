---
Title: "Elindult az FSF.hu Blog"
Date: 2010-02-21T20:00:00+02:00
Draft: false
Type: "post"
Summary: "Elindult az FSF.hu Blog"
Slug: "FSFhuBlog"
Author: "Admin"
---
Elindult az FSF.hu Alapítvány aktivistáinak közösségi blogja, a 
https://blog.fsf.hu. Az oldal célja, hogy bemutassa az alapítvány aktivistáinak
munkáját és életét az érdeklődők számára. Reményeink szerint az oldal
betekintést nyújt majd a magyar szabad szoftveres közösség életébe, és egyben
ahhoz is hozzájárul majd, hogy a népszerű projektek mögött álló embereket is
megismerhessék az oldal látogatói. Ennek megfelelően a szakmai írások mellett
terveink szerint személyesebb hangvételű bejegyzések is megjelennek majd.

Az oldal arra is lehetőséget biztosít a látogatók számára, hogy hozzászóljanak
a bejegyzésekhez, így bárki elmondhatja a véleményét egy-egy téma kapcsán,
vagy éppen kérdést tehet fel. Nem ez az egyetlen publikus csatorna, ami
lehetőséget biztosít az FSF.hu Alapítvánnyal kapcsolatos eszmecserére:
a szabad szoftveres körökben nagy népszerűségnek örvendő identi.ca mikroblogon
megtalálható fsfhu csoport üzeneteit bárki követheti, vagy akár írhat is oda.
Bízunk benne, hogy ez a két csatorna segít majd abban, hogy az érdeklődő
közönség jobban megismerhesse a munkánkat és minket.

