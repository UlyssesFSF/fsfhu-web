---
Title: "Fejlesztési beszámoló"
Date: 2010-03-02T20:00:00+02:00
Draft: false
Type: "post"
Summary: "Fejlesztési beszámoló"
Slug: "FejlesztésiBeszámoló"
Author: "Németh László"
---

**Kiadások:**

A _Kiadványkészítés OpenOffice.org Writer szövegszerkesztővel_ könyv 0.1-es
változata a bejelentésben:

 - http://libreoffice.hu/kiadvanykeszites-openoffice-org-writerrel-elozetes
 - https://hup.hu/cikkek/20100301/kiadvanykeszites_openoffice.org_writerrel_elozetes

A visszajelzés pozitív. Szabó Péter, a kiváló Magyar nyelvű műszaki–tudományos
tipográfia szerzője  is megkeresett (személyesen 2006-ban ismerkedtünk meg a
debreceni EuroTeX konferencián). Péter sok más mellett javasolta a címsor
behúzásának megváltoztatását. Ezzel nekem is gondom volt, de megláttam  egy
orvosi szövegek helyesírásával foglalkozó kiadványban a következő sávos címsor
és bekezdésformázást, amit az OpenOffice.org is képes automatikusan kezelni a
bekezdésstílusokkal (nem magától értetődő módon, hanem háttérképként, de
tipográfiai pontra pontosan az adott helyre és méretre téve a fekete sávot): 
http://numbertext.org/tmp/teszt.pdf. Ezt a megoldást Péter is sokkal 
elfogadhatóbbnak találta. A Hunspell forráskódját CVS-be tette a SouceForge-on 
Caolan McNamara (Red Hat). Hozzáadta a Red Hat Hunspell foltjait, én is
beletettem pár javítást, pl. a következő hibára: 
http://www.openoffice.org/issues/show_bug.cgi?id=106267

Még lesz benne egy-két javítás, és ki lesz adva hivatalosan is, de a CVS-ből
már a javított kód érhető el. (Nagyon jó hír, mivel az utóbbi egy évben nem
volt kiadás, pedig olyan vezető cégek is, mint az Apple és a Trados, építették
be a Hunspellt termékeikbe.)

Készül egy nem német umlautos, a betűváltozatokban jobban egalizált, az azokból
helyenként hiányzó fj/ffj ligatúrákat is tartalmazó változatot a Linux
Libertine betűkészletből, amivel a magyar szakszövegek igényes és nyílt
forráskódú eszközökkel és betűkészletekkel történő szedése is jobban
megvalósítható lesz.

Az új elválasztási programkönyvtárat [bejelentettem a Sunnál is][1].

Rene Engerhard (Debian) meg is nézte már, ő volt, aki anno külön szedte a
Hyphen és Hunspell programkönyvtárakat az OpenOffice.org-ban, hogy könnyebben
frissíthetők legyenek. A frissítés több helyen szükséges, mivel a
Linux-terjesztések OpenOffice.org-jai általában rendszerszintű elválasztási
programkönyvtárat és helyesírás-ellenőrzőt tartalmaznak.

A [Lightproof 1.4][2] is kiadásra került az új magyar nyelvi szabályok forrásával.

(Ha még a nemzetközi bejelentés is megtörténne végre, közelebb kerülne a
Lightproof az OpenOffice.org-integrációhoz, de ehhez nem ártana az angol
nyelvi ellenőrző modult kicsit összekapni. Az amerikai angol elválasztási
szótárból készült egy Unicode-ligatúrás változat is, azzal és a Hunspell-lel
együtt be lesz jelentve valamikor a közeljövőben terveim szerint az
OpenOffice.org Lingucomponent levelezési listáján.)

Kami és Kéménczy Kálmán a Go-oo és OpenSUSE irányában frissítették a magyar
szótárakat. Tímár András a *hu_dicts* OpenOffice.org kiterjesztésben és a
Mozilla AMO-ban (ahol most kivételesen gyorsan ellenőrizték a kiterjesztést).

Bővített magyar elválasztási minták az unicode-os f-ligatúrát tartalmazó szavak
elválasztásával (nem választ el hibás helyen ligatúrák esetén sem). A minták
megint Unicode (UTF-8) karakterkódolásúak, mivel sikerült unicode-osítani a
feltételes elválasztójelek beszúrását segítő párbeszédablakot Orvietóból
visszafele):
https://sourceforge.net/projects/magyarispell/files/OOo%20Huhyphn/v20060713-0.2/

[Hyphen 2.5][3] elválasztási programkönyvtár az unicode-os elválasztás és a
ligatúrát tartalmazó szavak elválasztásának javításával. A ligatúrás fejlesztés
derült ki, hogy bizonyos szavaknál, pl. „miért”,  hiányozhat az első
elválasztási pont, mindez az unicode-os lefthyphenmin, vagyis a balról vett
minimális elválasztási távolságot kezelő függvény hibája miatt. A ligatúrák
támogatása annyit tesz, hogy három betűt helyettesítő ligatúrák két karakternek
számítanak a hyphenmin értékek számításánál (a kétkarakteresek egyet, ami nem
igényelt külön fejlesztést, de tesztelési céllal beállítható egy makró, hogy az
unicode-os f-ligatúrák annyi karakternek számítsanak, mint amennyit
helyettesítenek, pl. az fi-ligatúra kettőt, az ffi-ligatúra hármat). Mindez
azért, hogy a fi-atal elválasztás ligatúra esetén ne működjön, de az
effi-ciency igen lefthyphenmin=3 elválasztási küszöb esetén is).
Ehhez még az OpenOffice.org oldaláról is javítani kell a kódot valószínűleg.

A Szabad magyar szótár új változatának bejelentése is késik, pedig igen
jelentősen bővült a szótár. Addig is itt vannak az állományok és a változások
listája:
[https://sourceforge.net/projects/magyarispell/files/Magyar Ispell/1.6/](https://sourceforge.net/projects/magyarispell/files/Magyar%20Ispell/1.6/)

[1]: http://www.openoffice.org/issues/show_bug.cgi?id=109543
[2]: http://libreoffice.hu/lightproof-1-4
[3]: https://sourceforge.net/projects/hunspell/files/Hyphen/2.5/
