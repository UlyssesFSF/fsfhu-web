---
Title: "IV. OpenScope este"
Date: 2018-02-26T20:14:20+02:00
Draft: false
Type: "post"
Summary: "IV. OpenScope este"
Slug: "OpenScope4"
Author: "Meskó Balázs"
---

Az <em>OpenScope esték</em> az FSF.hu alapítvány honosítással foglalkozó eseménysorozata. Az eseményre egyaránt várjuk a tapasztalt fordítókat, és a teljesen kezdőket is. Mindenki megtalálja a neki való feladatot, csatlakozzatok bátran!

A negyedik este 2018. március 1.-jén lesz, 18:00-kor, a magyar Mozilla közösség jóvoltából a [D18 Irodaházban][1]. Aktuális témánk a [Transportr][2] androidos alkalmazás, amely egy szabad tömegközlekedési alkalmazás. Érdekesség lesz, hogy a szoftver fordítása mellett itt a honosítási folyamat része a magyarországi tömegközlekedési adatok betöltése is a szoftverbe. Gyertek, és kapcsolódjatok be a munkánkba.

A rendezvény pontos helyszíne: **1066 Budapest, Dessewffy utca 18-20.**

*Bejutás a helyszínen: gyertek be a kávézóba, és a pult mellett baloldalt elhaladva menjetek le a lépcsőn, majd az alagsor vége felé gyertek be a jobb oldalon lévő tárgyalóba. Ha esetleg később érkeznétek, és már zárva a kávézó, akkor hívjátok a +36-20/432-5328-as telefonszámot.*

Ha esetleg kérdésetek van, akkor a következő helyeken értek el minket:
- [Telegram csoport][3]
- [Facebook oldal][4]

Ott találkozunk!

Meskó Balázs,
szervező

[1]: http://d18.hu
[2]: https://github.com/grote/Transportr
[3]: https://t.me/joinchat/DIfJOgiG22VjI1f616aZWA
[4]: https://facebook.com/openscope.org
