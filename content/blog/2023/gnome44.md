---
Title: "Megjelent a GNOME 44, az eddigi legjobb magyar támogatással"
Date: 2023-03-22T15:50:00+02:00
Draft: false
Type: "post"
Summary: ""
Slug: "gnome44"
Author: "Úr Balázs"
---

Az [előzetes ütemterv](https://wiki.gnome.org/FortyFour) szerint szerdán megjelent a GNOME 44, amely az eddigi legjobb magyar támogatással rendelkezik. A korábbi kiadásokhoz hasonlóan a felhasználói felület 100%-ban lefordított, és ebben a kiadásban az FSF.hu Alapítvány önkénteseinek köszönhetően a felhasználói útmutató is teljes egészében elérhető magyar nyelven.

A GNOME asztali környezetet számos Linux disztribúció használja alapértelmezett asztali környezetként, valamint a többinél is elérhető mint választható összetevő. Az asztali környezetek közül egyedül a GNOME rendelkezik teljes egészében magyarra lefordított felhasználói felülettel és dokumentációval. Ez nagyon fontos feltétel a magyar felhasználóknak, mert sokan csak az anyanyelvükön beszélnek.

A GNOME az Ubuntu Linux alapértelmezett asztali környezete, és az Ubuntu szolgál az FSF.hu Alapítvány támogatásával karbantartott Ubuntu Érettségi Remix alapjául. A mostantól elérhető magyar nyelvű felhasználói útmutató segítséget nyújt a rendszer használatának elsajátításában, így bátra ajánljuk mindenkinek, aki ezzel a rendszerrel szeretne érettségi vizsgát tenni. A felhasználói útmutató a tevékenységek áttekintésből a Súgó szó beírásával érhető el.

A magyar nyelvű felhasználói útmutató idősebb emberek – szüleink, nagyszüleink – számára is hatalmas segítség. Az ő számítógépüket jellemzően a gyermekeik tartják karban. Mindenképp javasoljuk az új felhasználói kézikönyvvel való ismerkedést, mert a számítógép hatékonyabb használatát teszi lehetővé a benne található ismeretek elsajátítása. Az újdonságokról a [kiadási megjegyzésben](https://release.gnome.org/44/) lehet olvasni (angol nyelven).

A GNOME 44 és a vele érkező magyar nyelvű felhasználói útmutató a 2023 áprilisában megjelenő Fedora 38 és Ubuntu 23.04 disztribúciókban lesz elérhető. Az LTS (hosszan támogatott) Ubuntu verziót használóknak sajnos várniuk kell még egy évet az Ubuntu 24.04 megjelenéséig.