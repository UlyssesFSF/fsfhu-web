---
Title: "Elkészült a Qt 6.6 honosítása"
Date: 2023-11-11T14:00:00+01:00 (publikálás ISO8601 időbélyege)
Draft: true (amíg csak vázlat, addig true)
Type: "post" (enélkül nem jelenik meg az RSS-hírcsatornában)
Summary: "Elkészült a Qt 6.6 honosítása"
Slug: "qt66"
Author: "Kiszel Kristóf (ulysses)"
---

Az FSF.hu Alapítvány támogatásával az elmúlt hónapokban elkészült a Qt 6.6-os ágának honosítása. A munka magában foglalta a meglévő fordítások teljes ellenőrzését és javítását, a hiányzó szövegek fordítását, majd a kész fájlok feltöltését a Qt kódáttekintő rendszerébe, a [Gerritbe](https://codereview.qt-project.org/c/qt/qttranslations/+/503776). A beolvasztás előtt szükség volt legalább két szavazatra magyar közreműködőktől, ebben Meskó Balázs és Gyuris Gellért segített.

A feladat nagysága miatt egyesével történt a fájlok fordítása és feltöltése, hogy az ellenőrzés is kisebb adagokban történhessen meg, ne egyszerre zúduljon sok szöveg a jóváhagyókra. Sajnos a fordítás csúszása miatt a 6.6-os végső kiadása hamarabb jelent meg, minthogy beolvasztásra került volna a fordítása, de a november 23-án érkező 6.6.1-es verzióban már elérhető lesz, illetve a Linux disztribúciók a csomagok készítése során is már a teljes magyar honosítást fogják a felhasználóknak nyújtani.